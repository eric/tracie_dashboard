#!/bin/sh

# Copyright (c) 2019 Collabora Ltd
# Copyright © 2020 Valve Corporation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

set -x

echo "$@"
OPTS=$(getopt -o p:i:d -l host-port:,docker-image:,docker-cmd: -n 'start-docker.sh' -- "$@")

echo "$OPTS"
eval set -- "$OPTS"

HOST_PORT=80
DOCKER_IMAGE="tracie_dashboard:latest"
DOCKER_CMD="docker"

while true; do
    case "$1" in
        -p|--host-port) HOST_PORT="$2"; shift; shift; ;;
        -i|--docker-image) DOCKER_IMAGE="$2"; shift; shift; ;;
        -d|--docker-cmd) DOCKER_CMD="$2"; shift; shift; ;;
        --) shift; break; ;;
        *) break; ;;
    esac
done

# Change to script's directory
cd "$(dirname "$(readlink -f "$0")")"

. ./setup-env.sh

"$DOCKER_CMD" run --rm -p $HOST_PORT:80 \
              -e DJANGO_SECRET_KEY="$DJANGO_SECRET_KEY" \
              -e DJANGO_ALLOWED_HOST="$DJANGO_ALLOWED_HOST" \
              -e DJANGO_GITLAB_TOKEN="$DJANGO_GITLAB_TOKEN" \
              -e DJANGO_GITLAB_URL="$DJANGO_GITLAB_URL" \
              -e DJANGO_MINIO_URL="$DJANGO_MINIO_URL" \
              -e DJANGO_DEBUG="$DJANGO_DEBUG" \
              --name tracie_dashboard \
              "$DOCKER_IMAGE"
