# Copyright (c) 2019 Collabora Ltd
# Copyright © 2020 Valve Corporation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import bz2
import collections
import gitlab
import os
import requests
import yaml

try:
    import simplejson as json
except ImportError:
    import json

from django.conf import settings

gl = None


def _connect_to_ci(project_path):
    global gl
    if gl is None:
        gl = gitlab.Gitlab(settings.GITLAB_URL, private_token=settings.GITLAB_TOKEN)
    return gl.projects.get(project_path)

class Job:
    __slots__ = ('id', 'project_path', 'pipeline_id', 'status', 'timestamp', 'device',
                 'trace_results')
    def __init__(self):
        self.id = None
        self.project_path = None
        self.pipeline_id = None
        self.status = None
        self.device = None
        self.timestamp = None
        self.trace_results = {}

    def url(self):
        return settings.GITLAB_URL + "/" + self.project_path + "/-/jobs/" + str(self.id)


    def _create_test_result_from_piglit_result(self, test_name, test_status, test_images):
        split_test_name = test_name.split('@')
        name = '/'.join(split_test_name[2:])
        expected, _ = os.path.splitext(test_images['image_ref'])
        if test_status == 'pass':
            actual = expected
        elif test_status == 'crash':
            actual = test_images['image_render']
        else:
            root, _ = os.path.splitext(test_images['image_render'])
            actual = root.split('-')[-1]

        return { name: {'actual': actual, 'expected': expected }}

    _MISSING_IMAGES = { "image_ref": "missing_ref", "image_render": "missing_render" }

    def populate_trace_results(self):
        if self.status == 'running' or self.status == 'pending':
            self.trace_results = {}
            return

        r = requests.get(settings.MINIO_URL + "/artifacts/{}/{}/{}/traces/results.json.bz2".format(self.project_path,
                                                                                                   self.pipeline_id,
                                                                                                   self.id))

        if r.status_code == 200:
            piglit_results = json.loads(
                bz2.decompress(r.content),
                object_pairs_hook=collections.OrderedDict)

            self.trace_results = {}

            for k, v in piglit_results['tests'].items():
                # If we don't have valid image information, use a fake entry
                # to avoid crashing.
                self.trace_results.update(
                    self._create_test_result_from_piglit_result(
                        k, v['result'],
                        v['images'][0] if v['images'] else self._MISSING_IMAGES))

            for k in piglit_results['tests'].keys():
                split_test_name = k.split('@')
                self.device = split_test_name[1]
                break

            return

        r = requests.get(settings.MINIO_URL + "/artifacts/{}/{}/{}/traces/results.yml".format(self.project_path,
                                                                                              self.pipeline_id,
                                                                                              self.id))
        if r.status_code == 200:
            self.trace_results = yaml.safe_load(r.content)
            return

        # TODO: Should we fail in any way if there are no results to
        # download or the transfer is failing?

class Pipeline:
    __slots__ = ('id', 'project_path', 'status', 'mesa_repo', 'mesa_sha',
                 'test_job_ids')
    def __init__(self):
        self.id = None
        self.project_path = None
        self.status = None
        self.mesa_repo = None
        self.mesa_sha = None
        self.test_job_ids = []

    def url(self):
        return settings.GITLAB_URL + "/" + self.project_path + "/pipelines/" + str(self.id)

def get_pipeline_ids(project_path, count=20):
    """Gets the latest 'count' pipeline ids"""

    project = _connect_to_ci(project_path)

    # Create a generator for the pipeline ids. python-gitlab fetches
    # results as needed in batches of 'per_page' (with 100 being the max).
    pipelines_gen = project.pipelines.list(as_list=False, per_page=min(100, count))

    return [p.id for _, p in zip(range(count), pipelines_gen)]

def get_pipeline(project_path, pid):
    project = _connect_to_ci(project_path)

    p = project.pipelines.get(pid)

    pipeline = Pipeline()
    pipeline.id = p.id
    pipeline.project_path = project_path
    pipeline.status = p.status
    pipeline.mesa_repo = "%s/%s" % (settings.GITLAB_URL, project_path)
    pipeline.mesa_sha = p.sha

    for j in p.jobs.list(all=True):
        if "-traces" in j.name or "_traces" in j.name:
            pipeline.test_job_ids.append(j.id)

    return pipeline

def get_test_job(project_path, jid):
    project = _connect_to_ci(project_path)

    job = project.jobs.get(jid)

    test_job = Job()
    test_job.id = jid
    test_job.project_path = project_path
    test_job.pipeline_id = job.pipeline['id']
    test_job.status = job.status
    test_job.timestamp = job.finished_at
    test_job.populate_trace_results()
    if not test_job.device:
        test_job.device = job.name.replace("-traces", "").replace("_traces", "").split(":")[0]

    return test_job
